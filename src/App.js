import React, { Component } from 'react';
import Form from "react-jsonschema-form";
import './App.css';
import CustomSelect from "./Components/CustomSelect/CustomSelect";

const schema = {
  title: "",
  type: "object",
  properties: {
    account: {
      title: "Расчетный счет",
      type: "number",
      enum: [123456789, 987654321, 23456788],
      enumNames: [123456789, 987654321, 23456788]
    }
  }
};

const uiSchema = {
  account: {
    "ui:placeholder": "Выберете расчетный счет",
    "ui:widget": CustomSelect,
    "ui:options": {
      label: false
    }
  }
};

const formData = {
  account: 123456789
};

const widgets = {
  AccountSelect: CustomSelect
};

class App extends Component {
  render() {
    return (
      <div className="App">
        <Form schema={schema}
              formData={formData}
              uiSchema={uiSchema}
              widgets={widgets}
        >
          <button type="submit">Сохранить</button>
        </Form>
      </div>
    );
  }
}

export default App;
