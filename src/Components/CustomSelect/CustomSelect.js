import React, { Component } from 'react';
import './CustomSelect.css';
import Select from "react-select";

const customStyles = {
  option: (provided, { data, isFocused, isSelected }) => {
    return {
      cursor: 'default',
      background: 'none',
      color: (isFocused || isSelected) ? '#3c91d7' : '#cccccc',
      padding: 5,
      'font-size': '14px'
    };
  },
  valueContainer: (provided) => {
    return {
      ...provided,
      'padding-left': 0
    };
  },
  placeholder: () => ({
    'font-size': '16px',
    color: '#cccccc'
  }),
  control: (provided, {isFocused}) => {
    return {
      ...provided,
      'box-shadow': 'none',
       border: 'none',
      'border-bottom': isFocused ? '1px solid #3c91d7' : '1px solid #cccccc',
      'border-radius': 0,
      'background': 'none',
      'height': '27px',
      'min-height': '27px'
    };
  },
  indicatorSeparator: () => ({
    display: 'none'
  }),
  menu: () => ({
    width: '350px',
    border: 'none',
    position: 'absolute',
    'background': 'white',
    '-webkit-box-shadow': '0px 0px 14px 0px rgba(143,143,143,1)',
    '-moz-box-shadow': '0px 0px 14px 0px rgba(143,143,143,1)',
    'box-shadow': '0px 0px 14px 0px rgba(143,143,143,1)',
    'padding': '10px'
  }),
  noOptionsMessage: () => ({
    display: 'none'
  }),
  clearIndicator: () => ({
    display: 'none'
  })
};

class CustomSelect extends Component {

  constructor(props, context) {
    super(props, context);
    this.handleChange = this.handleChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleFocus = this.handleFocus.bind(this);

    this.state = {
      selectValue: props.value,
      focused: false
    }
  }

  handleChange(selected) {
    this.selectRef.blur();
    this.setState({selectValue: selected}, () => {
      this.props.onChange(selected.value);
    })
  }

  handleFocus() {
    this.setState({focused: true});
  }

  handleBlur() {
    this.setState({focused: false});
  }

  render() {
    const {placeholder, options, schema} = this.props;
    const {focused, selectValue} = this.state;
    return (
      <div className="select-field-wrap">
        <label className={"select-field-label " + (focused ? 'focused' : '')}>{schema.title}</label>
        <Select
          ref={el => this.selectRef = el}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          value={selectValue}
          placeholder={placeholder}
          styles={customStyles}
          options={options ? options.enumOptions : []}
        />
      </div>
    );
  }
}

export default CustomSelect;